# Pomodoro

A command line Python Pomodoro timer with seasonal emoji counters. The Pomodoro Technique is a time management method developed by Francesco Cirillo in the late 1980s. It uses a kitchen timer to break work into intervals, typically 25 minutes in length, separated by short breaks. Each interval is known as a pomodoro, from the Italian word for tomato, after the tomato-shaped kitchen timer Cirillo used as a university student.

1.  Set the pomodoro timer, 25 minutes.
2. Work on the task.
3. End work when the timer rings and take a short break, 5 minutes.
4. If you have finished fewer than three pomodoros, go back to Step 1 and repeat until you go through all three pomodoros.
5. After three pomodoros are done, take the fourth pomodoro and then take a long break, 30 minutes.