"""
Description: Pomodoro Timer
Version: 1.0
Author: Scott Buffington
Beerware License
A Bufflabs Project - <irongeek@pm.me> wrote this program. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think this is worth it,
you can buy me a beer in return.
"""

import os
import textwrap
import time
from time import strftime

# countdown timer 
def countdown(t):
    while t: # while t > 0 for clarity 
      mins = t // 60
      secs = t % 60
      timer = '{:02d}:{:02d}'.format(mins, secs)
      print(timer, end="\r") # overwrite previous line 
      time.sleep(1)
      t -= 1
    print('Start Planting!')

# define season
def get_season(month):
    seasons = {
    '🎃': ['September', 'October'],
    '🍗': ['November'],
    '🎄': ['December'],
    '🎉': ['January'],
    '🍅': ['February', 'March', 'April', 'May', 'June', 'July', 'August']
    }

    for season in seasons:
        if month in seasons[season]:
            return season
    return 'Invalid input month'


# pomodoro timer 
def pomodoro(): 
  print("Pomodoro starts now!\a")
  count = 0
  current_month = strftime('%B')
  season_emoji = get_season(current_month)
  try:
    while True:
      t = 25*60
      while t: 
        mins = t // 60 
        secs = t % 60
        timer = '{:02d}:{:02d}'.format(mins, secs) 
        print(timer, end="\r") # overwrite previous line 
        time.sleep(1)
        t -= 1 
      count += 1
      os.system('clear')
      make_display_row = season_emoji * count
      print(textwrap.fill(make_display_row, 4))
      print("Break Time!!\a\a\a")
      time.sleep(4)
      if count % 4 == 0:
        t = 30*60
      else:
        t = 5*60 
      while t: 
        mins = t // 60 
        secs = t % 60
        timer = '{:02d}:{:02d}'.format(mins, secs) 
        print(timer, end="\r") # overwrite previous line 
        time.sleep(1)
        t -= 1 
      os.system('clear')
      make_display_row = season_emoji * count
      print(textwrap.fill(make_display_row, 4))
      print("Work Time!!!\a\a\a")
      time.sleep(4)
  except KeyboardInterrupt:
    pass

t = input("Enter the time in seconds until pomodoro timer whould start: ")
countdown(int(t))
os.system('clear')
pomodoro()


